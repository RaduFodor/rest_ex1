package com.example.demo;

public class Customer {
	private String name;
	private int age;
	private long id;
	//@return the name
	public String getName() {
		return name;
	}
	//@param name the name to set
	 public void setName(String name) {
		this.name = name;
	}
	//@return the age
	 public int getAge() {
		return age;
	}
	//@param age the age to set
	public void setAge(int age) {
		this.age = age;
	}
	//@return the id
	 public long getId() {
		return id;
	}
	//@param id the id to set
	 public void setId(long id) {
		this.id = id;
	}
	
	 public Customer() {
		 
	 }
	 
	 public Customer(long id, String name, int age) {
			this.name = name;
			this.age = age;
			this.id = id;
		}
	 
	//override in Controller methods
	 public String toString() {
		 String info = String.format("Customer Info: id = %d, name = %s, age = %d",
                 id, name, age);
		 return info;
	}

}
